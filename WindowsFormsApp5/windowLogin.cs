﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;




namespace WindowsFormsApp5
{
    public partial class windowLogin : Form
    {

        Thread th;
        
        
        static User user1 = new User("qwerty99", "ytrewq11");
        static User user2 = new User("mthtak", "1234");
        static User user3 = new User("ömeralk", "ömr12");
        static User[] users = new User[] { user1,user2,user3};
        
        public windowLogin()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool loggedIn = false;
            int usersLength = users.Length;
            for(int i = 0; i < usersLength; i++) { 
                  if(usernameTextBox.Text == users[i].username && passwordTextBox.Text == users[i].password)
                  {
                      uyariMesaji.Text = "You are successfully logged in";
                      uyariMesaji.BackColor = Color.LightGreen;
                      loggedIn = true;
                      break;                  //breaks when all conditions above are true
                  }  
                  else
                  {
                      uyariMesaji.Text = "Wrong username or password";
                      uyariMesaji.BackColor = Color.IndianRed;
                  }

            }
           
            if (loggedIn)
            {
                timer1.Start();           //if logged in, timer starts counting
            }

            

        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            passwordTextBox.PasswordChar = '*';          //password should not to be visible
        }
        private int ticks;
        private void timer1_Tick(object sender, EventArgs e)
        {
            ticks++;
            if (ticks == 3)
            {
                this.Close();
                th = new Thread(openNewForm);
                th.SetApartmentState(ApartmentState.STA);
                th.Start();

            }

        }

        private void openNewForm(object obj)
        {
            Application.Run(new windowCalc());        //it will come from other team made
        }
    }
}
