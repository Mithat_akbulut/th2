﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class windowCalc : Form
    {

        int buttonOrder = 0;        //sumButton = 1, subtractButton =2, multipleButton = 3,divideButton=4
        public windowCalc()
        {
            InitializeComponent();
        }
        
        private void sumButton_Click(object sender, EventArgs e)
        {
            buttonOrder = 1;
            if (!checkInputsNull())
            {
                calcultor();
                double sonuc = calcultor().sumElements(calcultor().number1, calcultor().number2);
                sonucLabel.Text = sonuc.ToString();
                changeLabelBackground(isGreaterThanZero(sonuc));
                whichIsColored(buttonOrder);
            }
            else
            {
                MessageBox.Show("you must fill the input spaces before calculate");
            }
                
        }

        private void subtractButton_Click(object sender, EventArgs e)
        {
            buttonOrder = 2;
            if (!checkInputsNull())
            {
                calcultor();
                double sonuc = calcultor().extractElements(calcultor().number1, calcultor().number2);
                sonucLabel.Text = sonuc.ToString();
                changeLabelBackground(isGreaterThanZero(sonuc));
                whichIsColored(buttonOrder);
            }
            else
            {
                MessageBox.Show("you must fill the input spaces before calculate");
            }
                
        }

        private void multipleButton_Click(object sender, EventArgs e)
        {
            buttonOrder = 3;
            if (!checkInputsNull())
            {
                calcultor();
                double sonuc = calcultor().multipleElements(calcultor().number1, calcultor().number2);
                sonucLabel.Text = sonuc.ToString();
                changeLabelBackground(isGreaterThanZero(sonuc));
                whichIsColored(buttonOrder);
            }
            else
            {
                MessageBox.Show("you must fill the input spaces before calculate");
            }
            
        }

        private void divideButton_Click(object sender, EventArgs e)
        {
            buttonOrder = 4;
            if (!checkInputsNull())
            {
                if(sayi2Input.Text =="0")
                {
                    sonucLabel.Text = "divide by zero error: " + sayi1Input.Text + "/0";
                }
                else
                {
                    calcultor();
                    double sonuc = calcultor().divideElements(calcultor().number1, calcultor().number2);
                    sonucLabel.Text = sonuc.ToString();
                    changeLabelBackground(isGreaterThanZero(sonuc));
                    whichIsColored(buttonOrder);
                }
                
            }
            else
            {
                MessageBox.Show("you must fill the input spaces before calculate");
            }
        }

        public static void sayi1Input_TextChanged(object sender, EventArgs e)
        {

        }

        private void sayi2Input_TextChanged(object sender, EventArgs e)
        {

        }

        private Calculator calcultor()         //creates and returns a Calculator object
        {
            double num1 = double.Parse(sayi1Input.Text);
            double num2 = double.Parse(sayi2Input.Text);
            Calculator calculator = new Calculator(num1, num2);
               
            return calculator;
            
        }
        private bool checkInputsNull()               //it's important to check whether input is null or not
        {
            bool isNull = false;
            if(String.IsNullOrEmpty(sayi1Input.Text) || String.IsNullOrEmpty(sayi2Input.Text))
            {
                isNull = true;
            }

            return isNull;

            
        }

        private bool isGreaterThanZero(double sonuc)         //it's used for changing result's background thing
        {
            bool isGreater = false;

            if (sonuc > 0)
            {
                isGreater = true;
            }
            return isGreater;
        }

        private void changeLabelBackground(bool a)             //a function that takes a bool parameter for knowing that if result is greater than zero 
        {
           if(a == true)
            {
                sonucLabel.BackColor = Color.LightGreen;
            }
            else
            {
                sonucLabel.BackColor = Color.IndianRed;
            }
        }

        private void whichIsColored(int order)    //with the int parameter we will know that which button is clicked last
        {
            if(order == 1)
            {
                sumButton.BackColor = Color.Aquamarine;
                subtractButton.BackColor = Color.White;
                multipleButton.BackColor = Color.White;
                divideButton.BackColor = Color.White;
            }
            if(order == 2)
            {
                subtractButton.BackColor = Color.Aquamarine;
                sumButton.BackColor = Color.White;
                multipleButton.BackColor = Color.White;
                divideButton.BackColor = Color.White;
            }
            if (order == 3)
            {
                subtractButton.BackColor = Color.White;
                sumButton.BackColor = Color.White;
                multipleButton.BackColor = Color.Aquamarine;
                divideButton.BackColor = Color.White;
            }
            if (order == 4)
            {
                subtractButton.BackColor = Color.White;
                sumButton.BackColor = Color.White;
                multipleButton.BackColor = Color.White;
                divideButton.BackColor = Color.Aquamarine;
            }
        }

        






        private void sumButton_MouseEnter(object sender, EventArgs e)
        {
            sumButton.ForeColor = Color.Red;
        }

        private void sumButton_MouseLeave(object sender, EventArgs e)
        {
            sumButton.ForeColor = Color.Black;
        }

        private void subtractButton_MouseEnter(object sender, EventArgs e)
        {
            subtractButton.ForeColor = Color.Red;
        }
        private void subtractButton_MouseLeave(object sender, EventArgs e)
        {
            subtractButton.ForeColor = Color.Black;
        }
        private void multipleButton_MouseEnter(object sender, EventArgs e)
        {
            multipleButton.ForeColor = Color.Red;
        }
        private void multipleButton_MouseLeave(object sender, EventArgs e)
        {
            multipleButton.ForeColor = Color.Black;
        }
        private void divideButton_MouseEnter(object sender, EventArgs e)
        {
            divideButton.ForeColor = Color.Red;
        }
        private void divideButton_MouseLeave(object sender, EventArgs e)
        {
            divideButton.ForeColor = Color.Black;
        }

        private void windowCalc_Load(object sender, EventArgs e)
        {

        }
    }
}
